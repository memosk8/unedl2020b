﻿using System;

namespace TorresDeHanoi
{
    class Hanoi
    {
        void hanoi(int n, char i, char a, char f)
        {
            if (n == 1)
            {
                Console.WriteLine(i + " -> " + f);
                m++;
            }

            else
            {
                m++;
                hanoi(n - 1, i, f, a);
                Console.WriteLine(i + " -> " + f);
                hanoi(n - 1, a, i, f);
            }
        }

        int m = 0;
        static void Main(string[] args)
        {
            Hanoi h = new Hanoi();
            char inicio = 'A';
            char aux = 'B';
            char fin = 'C';
            int n;

            Console.WriteLine("Número de discos: ");
            n = int.Parse(Console.ReadLine();

            Console.WriteLine("\nMovimientos : \n");
            h.hanoi(n, inicio, aux, fin);
            Console.WriteLine("\nMovimientos requeridos : " + h.m + "\n");
            Console.ReadLine();
        }
    }
}
