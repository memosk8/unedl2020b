﻿using FluentValidation;
using FluentValidation.Results;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using Tienda.COMMON.Entidades;
using Tienda.COMMON.Interfaces;

namespace Tienda.DAL.MySQL
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseDTO
    {
        private DBMySQL db;
        private bool idEsAutonumerico;
        private AbstractValidator<T> validator;
        public GenericRepository(AbstractValidator<T> validator, bool idEsAutonumerico = true)
        {
            this.validator = validator;
            this.idEsAutonumerico = idEsAutonumerico;
            db = new DBMySQL();
        }
        public string Error { get; private set; }

        public IEnumerable<T> Read {
            get
            {
                try
                {
                    string sql = string.Format("SELECT * FROM {0};",typeof(T).Name);
                    MySqlDataReader r = (MySqlDataReader)db.Consulta(sql);
                    List<T> datos = new List<T>();
                    var campos = typeof(T).GetProperties();
                    T dato;
                    Type Ttypo = typeof(T);
                    while (r.Read())
                    {
                        dato = (T)Activator.CreateInstance(typeof(T));
                        for (int i = 0; i < campos.Length; i++)
                        {
                            PropertyInfo prop = Ttypo.GetProperty(campos[i].Name);
                            prop.SetValue(dato, r[i]);
                        }
                        datos.Add(dato);
                    }
                    r.Close();
                    Error = "";
                    return datos;
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    return null;
                }
            }
        }

        public bool Create(T entidad)
        {
            ValidationResult resultadoDeValidacion = validator.Validate(entidad);
            if (resultadoDeValidacion.IsValid)
            {
                string sql1 = "INSERT INTO " + typeof(T).Name + " (";
                string sql2 = ") VALUES (";
                var campos = typeof(T).GetProperties();
                T dato = (T)Activator.CreateInstance(typeof(T));
                Type Ttypo = typeof(T);
                for(int i = 0; i < campos.Length; i++)
                {
                    if(idEsAutonumerico && i == 0)
                    {
                        continue;
                    }
                    sql1 += " " + campos[i].Name;
                    var propiedad = Ttypo.GetProperty(campos[i].Name);
                    var valor = propiedad.GetValue(entidad);
                    switch (propiedad.PropertyType.Name)
                    {
                        case "String":
                            sql2 += "'" + valor + "'";
                            break;
                        case "DateTime":
                            DateTime v = (DateTime)valor;
                            sql2 += string.Format($"'{v.Year}-{v.Month}-{v.Day} {v.Hour}:{v.Minute}:00'");
                            break;
                        default:
                            sql2 += " " + valor;
                            break;
                    }
                    if(i != campos.Length - 1)
                    {
                        sql1 += " ,";
                        sql2 += " ,";
                    }
                }
                return EjecutarComando( sql1 + sql2 + ");" );
            }
            else
            {
                Error = "Error de validacion : ";
                foreach(var item in resultadoDeValidacion.Errors)
                {
                    Error += item.ErrorMessage + ". ";
                }
                return false;
            }
        }

        private bool EjecutarComando(string v)
        {
            throw new NotImplementedException();
        }

        public bool Delete(string id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<T> Query(Expression<Func<T>> predicado)
        {
            throw new NotImplementedException();
        }

        public T SearchById(string id)
        {
            throw new NotImplementedException();
        }

        public bool Update(T entidad)
        {
            throw new NotImplementedException();
        }
    }
}
