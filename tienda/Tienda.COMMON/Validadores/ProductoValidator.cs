﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using Tienda.COMMON.Entidades;

namespace Tienda.COMMON.Validadores
{
    public class ProductoValidator:AbstractValidator<producto>
    {
        public ProductoValidator()
        {
            RuleFor(u => u.Costo).NotNull().GreaterThan(0);
            RuleFor(u => u.Nombre).NotNull().NotEmpty().Length(1, 50);

        }
    }
}
