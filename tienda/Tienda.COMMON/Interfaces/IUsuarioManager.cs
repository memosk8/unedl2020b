﻿using System;
using System.Collections.Generic;
using System.Text;
using Tienda.COMMON.Entidades;

namespace Tienda.COMMON.Interfaces
{
    public interface IUsuarioManager: IGenericManager<usuario>
    {
        /// <summary>
        /// Varifica si las credenciales son validas para el usuario
        /// </summary>
        /// <param name="nombreUsuario">Nombre de usuario</param>
        /// <param name="password">contrasena de usuario</param>
        /// <returns>si las credenciales son correctas regresa el usuario completo, de lo contrario null</returns>
        usuario Login(string nombreUsuario, string password);
    }
}
