﻿using System;
using System.Collections.Generic;
using System.Text;
using Tienda.COMMON.Entidades;

namespace Tienda.COMMON.Interfaces
{
    public interface IVentasManager:IGenericRepository<venta>
    {
        IEnumerable<venta> VentasEnIntervalo(DateTime inicio, DateTime fin);
        IEnumerable<venta> VentasDeClienteEnIntervalo(string nombreCliente, DateTime inicio, DateTime fin);
    }
}
