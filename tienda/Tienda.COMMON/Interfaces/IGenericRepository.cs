﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Tienda.COMMON.Entidades;

namespace Tienda.COMMON.Interfaces
{
    /// <summary>
    /// Proporciona metodos basicos (CRUD) de acceso a una tabla de base de datos
    /// </summary>
    /// <typeparam name="T">Tipo de entidad (Clase) a la que se refiere una tabla</typeparam>
    public interface IGenericRepository<T> where T:BaseDTO
    {
        /// <summary>
        /// Proporciona informacion sobre el error ocurrido en alguna de las operaciones (CRUD)
        /// </summary>
        string Error { get; }

        /// <summary>
        /// Inserta una entidad en la tabla
        /// </summary>
        /// <param name="entidad">Entidad a insertar</param>
        /// <returns>confirmacion de la insercion</returns>
        bool Create(T entidad);
        /// <summary>
        /// Obtiene todos los registros de la tabla
        /// </summary>
        IEnumerable<T> Read { get; }

        /// <summary>
        /// Actualiza un registro en la tabla en base a la propiedad Id
        /// </summary>
        /// <param name="entidad">Entidad ya modificada, el Id debe existir en la tabla para modificarse</param>
        /// <returns>confirmacion de la actualizacion</returns>
        bool Update(T entidad);

        /// <summary>
        /// Elimina una entidad en la base de datos de acuerdo al Id relacionado
        /// </summary>
        /// <param name="id">El Id de la entidad a eliminar</param>
        /// <returns>Confirmacion de eliminacion</returns>
        bool Delete(string id);

        //Query -> Realizar consultas de acuerdo a la tabla, mediante expresiones lambda

        /// <summary>
        /// Realiza una consulta personalizada a la tabla
        /// </summary>
        /// <param name="predicado">Expresion lambda que define la consulta</param>
        /// <returns>conjunto de entidades que cumplen con la consulta</returns>
        IEnumerable<T> Query(Expression<Func<T>> predicado);

        /// <summary>
        /// Obtener una entidad en base a su Id
        /// </summary>
        /// <param name="id">Id de la entidad a obtener</param>
        /// <returns>Entidad completa que corresponde al Id proporcionado</returns>
        T SearchById(string id);
    }
}
