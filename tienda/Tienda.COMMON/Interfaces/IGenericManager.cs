﻿using System;
using System.Collections.Generic;
using System.Text;
using Tienda.COMMON.Entidades;

namespace Tienda.COMMON.Interfaces
{
    /// <summary>
    /// Proporciona metodos estandarizados para el acceso a tablas; cada manager creado debe implementar de esta interface
    /// </summary>
    /// <typeparam name="T">Tipo de entidad de la cual se implementa el manager</typeparam>
    public interface IGenericManager<T> where T : BaseDTO
    {
        string Error { get; }
        bool Insertar(T entidad);
        IEnumerable<T> ObtenerTodos { get; }
        bool Actualizar(T entidad);
        bool Eliminar(T entidad);
        T BuscarPorId(string id);
    }
}
