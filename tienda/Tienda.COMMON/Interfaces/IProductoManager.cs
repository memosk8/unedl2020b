﻿using System;
using System.Collections.Generic;
using System.Text;
using Tienda.COMMON.Entidades;

namespace Tienda.COMMON.Interfaces
{
    public interface IProductoManager:IGenericManager<producto>
    {
        IEnumerable<producto> BuscarProductosPorNombre(string criterio);

        producto BuscarProductoPorNombreExacto(string nombre);
    }
}
