﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta
{
    public class Producto
    {
        private string IdProducto;
        private string Nombre;
        private double peso_en_g;
        private double Costo;
        private double precioVenta;
        private string descripcion;
        private DateTime timestamp;

        public Producto()
        {
            timestamp = DateTime.Now;
        }

        public void setIdProducto(string id)
        {
            IdProducto = id;
        }

        public void setNombre(string nombre)
        {
            Nombre = nombre;
        }
        
        public void setPrecioVenta()
        {
            precioVenta = Costo + (Costo * 0.10);
        }

        public void setCosto(double costo)
        {
            Costo = costo;
        }

        public void setPeso(double peso)
        {
            peso_en_g = peso;
        }

        public void setDescripcion(string desc)
        {
            descripcion = desc;
        }

        public string getDescripcion()
        {
            return descripcion;
        }

        public string getNombre()
        {
            return Nombre;
        }

        public string getId()
        {
            return IdProducto;
        }

        public double getPeso()
        {
            return peso_en_g;
        }

        public double getCosto()
        {
            return Costo;
        }

        public double getPrecio()
        {
            return precioVenta;
        }

        public override string ToString() {
            return "Nombre : " + Nombre + ", ID : " + IdProducto +
                    ", Peso (g) : " + peso_en_g + ", Costo : " + Costo +
                    ", Precio de venta : " + precioVenta + ", Fecha de compra : " + timestamp;
        }

        public DateTime getTimeStamp()
        {
            return timestamp;
        }
    }

    

}
