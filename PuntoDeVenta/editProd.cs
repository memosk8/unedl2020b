﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PuntoDeVenta
{

    public partial class editProd : Form
    {
        List<Producto> productos = new List<Producto>();

        public editProd()
        {
            InitializeComponent();
        }

        private void btnCrearProd_Click(object sender, EventArgs e)
        {
            {
                Producto prod = new Producto();

                prod.setIdProducto(txtIdProd.Text);

                string path = @"Productos\" + prod.getId() + ".txt";
                if (!File.Exists(path))
                {
                    prod.setCosto(double.Parse(txtCostoProd.Text));
                    prod.setNombre(txtNomProd.Text);
                    prod.setPeso(double.Parse(txtPeso.Text));
                    prod.setPrecioVenta();
                    prod.setDescripcion(txtDesc.Text);

                    var row = new string[] {
                    prod.getNombre(),
                    prod.getId(),
                    prod.getCosto().ToString(),
                    prod.getPrecio().ToString(),
                    prod.getPeso().ToString(),
                    prod.getTimeStamp().ToString()
                };

                    var lvi = new ListViewItem(row);
                    lvi.Tag = prod;
                    
                    ListaProductos.Items.Add(lvi);
                    txtShowDesc.Text = prod.getDescripcion();

                    using (StreamWriter sw = File.CreateText(path))
                    {
                        sw.WriteLine("Nombre : " + prod.getNombre());
                        sw.WriteLine("ID : " + prod.getId());
                        sw.WriteLine("Peso (g) : " + prod.getPeso());
                        sw.WriteLine("Costo : " + prod.getCosto());
                        sw.WriteLine("Precio de venta : " + prod.getPrecio());
                    }

                    MessageBox.Show("Producto registrado correctamente");
                }
                else
                {
                    MessageBox.Show("El #ID ya existe en el registro,\n proporcione uno distinto");
                }
                //WriteCharacters();

            }
        }
    }
}
