﻿namespace PuntoDeVenta
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabVentas = new System.Windows.Forms.TabPage();
            this.splVentas = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.tabProductos = new System.Windows.Forms.TabPage();
            this.splProductos = new System.Windows.Forms.SplitContainer();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.txtDesc = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtPeso = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtCostoProd = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtIdProd = new System.Windows.Forms.TextBox();
            this.txtNomProd = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCrearProd = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtShowDesc = new System.Windows.Forms.TextBox();
            this.ListaProductos = new System.Windows.Forms.ListView();
            this.lstColNombre = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lstColId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lstColCosto = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lstColPrecio = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lstColPeso = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lstColFecha = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel5 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.tabCalendario = new System.Windows.Forms.TabPage();
            this.calendario = new System.Windows.Forms.MonthCalendar();
            this.tabControl1.SuspendLayout();
            this.tabVentas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splVentas)).BeginInit();
            this.splVentas.Panel1.SuspendLayout();
            this.splVentas.Panel2.SuspendLayout();
            this.splVentas.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel6.SuspendLayout();
            this.tabProductos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splProductos)).BeginInit();
            this.splProductos.Panel1.SuspendLayout();
            this.splProductos.Panel2.SuspendLayout();
            this.splProductos.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel5.SuspendLayout();
            this.tabCalendario.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabVentas);
            this.tabControl1.Controls.Add(this.tabProductos);
            this.tabControl1.Controls.Add(this.tabCalendario);
            this.tabControl1.Location = new System.Drawing.Point(16, 13);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(904, 665);
            this.tabControl1.TabIndex = 3;
            // 
            // tabVentas
            // 
            this.tabVentas.Controls.Add(this.splVentas);
            this.tabVentas.Location = new System.Drawing.Point(4, 25);
            this.tabVentas.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabVentas.Name = "tabVentas";
            this.tabVentas.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabVentas.Size = new System.Drawing.Size(896, 636);
            this.tabVentas.TabIndex = 0;
            this.tabVentas.Text = "Ventas";
            this.tabVentas.ToolTipText = "Ver y registrar ventas";
            this.tabVentas.UseVisualStyleBackColor = true;
            // 
            // splVentas
            // 
            this.splVentas.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splVentas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splVentas.IsSplitterFixed = true;
            this.splVentas.Location = new System.Drawing.Point(3, 4);
            this.splVentas.Name = "splVentas";
            // 
            // splVentas.Panel1
            // 
            this.splVentas.Panel1.Controls.Add(this.panel1);
            this.splVentas.Panel1.Controls.Add(this.panel4);
            this.splVentas.Panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer1_Panel1_Paint_1);
            // 
            // splVentas.Panel2
            // 
            this.splVentas.Panel2.Controls.Add(this.panel6);
            this.splVentas.Size = new System.Drawing.Size(890, 628);
            this.splVentas.SplitterDistance = 295;
            this.splVentas.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(1, 66);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(289, 555);
            this.panel1.TabIndex = 5;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.label4);
            this.panel4.Location = new System.Drawing.Point(2, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(288, 55);
            this.panel4.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label4.Font = new System.Drawing.Font("Myanmar Text", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(63, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(137, 29);
            this.label4.TabIndex = 0;
            this.label4.Text = "Registrar Ventas";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.label5);
            this.panel6.Location = new System.Drawing.Point(4, 2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(582, 55);
            this.panel6.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label5.Font = new System.Drawing.Font("Myanmar Text", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(221, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(157, 29);
            this.label5.TabIndex = 0;
            this.label5.Text = "Ventas Registradas";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // tabProductos
            // 
            this.tabProductos.Controls.Add(this.splProductos);
            this.tabProductos.Location = new System.Drawing.Point(4, 25);
            this.tabProductos.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabProductos.Name = "tabProductos";
            this.tabProductos.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabProductos.Size = new System.Drawing.Size(896, 636);
            this.tabProductos.TabIndex = 1;
            this.tabProductos.Text = "Productos";
            this.tabProductos.ToolTipText = "Ver y registrar productos";
            this.tabProductos.UseVisualStyleBackColor = true;
            // 
            // splProductos
            // 
            this.splProductos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splProductos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splProductos.IsSplitterFixed = true;
            this.splProductos.Location = new System.Drawing.Point(3, 4);
            this.splProductos.Name = "splProductos";
            // 
            // splProductos.Panel1
            // 
            this.splProductos.Panel1.Controls.Add(this.panel3);
            this.splProductos.Panel1.Controls.Add(this.panel2);
            // 
            // splProductos.Panel2
            // 
            this.splProductos.Panel2.Controls.Add(this.splitContainer1);
            this.splProductos.Panel2.Controls.Add(this.ListaProductos);
            this.splProductos.Panel2.Controls.Add(this.panel5);
            this.splProductos.Size = new System.Drawing.Size(890, 628);
            this.splProductos.SplitterDistance = 296;
            this.splProductos.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.button1);
            this.panel3.Controls.Add(this.txtDesc);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.txtPeso);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.txtCostoProd);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.txtIdProd);
            this.panel3.Controls.Add(this.txtNomProd);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.btnCrearProd);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(292, 624);
            this.panel3.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(156, 540);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(110, 54);
            this.button1.TabIndex = 13;
            this.button1.Text = "Seleccionar imagen";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // txtDesc
            // 
            this.txtDesc.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesc.Location = new System.Drawing.Point(31, 356);
            this.txtDesc.Multiline = true;
            this.txtDesc.Name = "txtDesc";
            this.txtDesc.Size = new System.Drawing.Size(235, 159);
            this.txtDesc.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(28, 334);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 19);
            this.label9.TabIndex = 11;
            this.label9.Text = "Descripción";
            // 
            // txtPeso
            // 
            this.txtPeso.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPeso.Location = new System.Drawing.Point(31, 276);
            this.txtPeso.Name = "txtPeso";
            this.txtPeso.Size = new System.Drawing.Size(185, 28);
            this.txtPeso.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(27, 254);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 19);
            this.label8.TabIndex = 9;
            this.label8.Text = "Peso (gr)";
            // 
            // txtCostoProd
            // 
            this.txtCostoProd.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCostoProd.Location = new System.Drawing.Point(32, 196);
            this.txtCostoProd.Name = "txtCostoProd";
            this.txtCostoProd.Size = new System.Drawing.Size(182, 28);
            this.txtCostoProd.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 174);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(146, 19);
            this.label7.TabIndex = 7;
            this.label7.Text = "Costo de Producto :";
            // 
            // txtIdProd
            // 
            this.txtIdProd.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdProd.Location = new System.Drawing.Point(32, 119);
            this.txtIdProd.Name = "txtIdProd";
            this.txtIdProd.Size = new System.Drawing.Size(181, 28);
            this.txtIdProd.TabIndex = 6;
            // 
            // txtNomProd
            // 
            this.txtNomProd.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNomProd.Location = new System.Drawing.Point(32, 47);
            this.txtNomProd.Name = "txtNomProd";
            this.txtNomProd.Size = new System.Drawing.Size(181, 28);
            this.txtNomProd.TabIndex = 5;
            this.txtNomProd.TextChanged += new System.EventHandler(this.txtNomProd_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(24, 97);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(122, 19);
            this.label6.TabIndex = 4;
            this.label6.Text = "ID de Producto :";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(164, 19);
            this.label1.TabIndex = 3;
            this.label1.Text = "Nombre de Producto :";
            // 
            // btnCrearProd
            // 
            this.btnCrearProd.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrearProd.Location = new System.Drawing.Point(32, 540);
            this.btnCrearProd.Name = "btnCrearProd";
            this.btnCrearProd.Size = new System.Drawing.Size(110, 54);
            this.btnCrearProd.TabIndex = 2;
            this.btnCrearProd.Text = "Guardar Producto";
            this.btnCrearProd.UseVisualStyleBackColor = true;
            this.btnCrearProd.Click += new System.EventHandler(this.btnCrearProd_Click_1);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(2, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(288, 55);
            this.panel2.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label2.Font = new System.Drawing.Font("Myanmar Text", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(63, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(162, 29);
            this.label2.TabIndex = 0;
            this.label2.Text = "Registrar Productos";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Location = new System.Drawing.Point(11, 47);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.pictureBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.AccessibleRole = System.Windows.Forms.AccessibleRole.StaticText;
            this.splitContainer1.Panel2.Controls.Add(this.txtShowDesc);
            this.splitContainer1.Size = new System.Drawing.Size(563, 215);
            this.splitContainer1.SplitterDistance = 187;
            this.splitContainer1.TabIndex = 6;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(187, 215);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // txtShowDesc
            // 
            this.txtShowDesc.Location = new System.Drawing.Point(2, 0);
            this.txtShowDesc.Multiline = true;
            this.txtShowDesc.Name = "txtShowDesc";
            this.txtShowDesc.ReadOnly = true;
            this.txtShowDesc.Size = new System.Drawing.Size(367, 212);
            this.txtShowDesc.TabIndex = 0;
            this.txtShowDesc.TextChanged += new System.EventHandler(this.txtShowDesc_TextChanged);
            // 
            // ListaProductos
            // 
            this.ListaProductos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ListaProductos.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lstColNombre,
            this.lstColId,
            this.lstColCosto,
            this.lstColPrecio,
            this.lstColPeso,
            this.lstColFecha});
            this.ListaProductos.FullRowSelect = true;
            this.ListaProductos.GridLines = true;
            this.ListaProductos.HideSelection = false;
            this.ListaProductos.Location = new System.Drawing.Point(11, 268);
            this.ListaProductos.Name = "ListaProductos";
            this.ListaProductos.Size = new System.Drawing.Size(560, 343);
            this.ListaProductos.TabIndex = 5;
            this.ListaProductos.UseCompatibleStateImageBehavior = false;
            this.ListaProductos.View = System.Windows.Forms.View.Details;
            this.ListaProductos.SelectedIndexChanged += new System.EventHandler(this.ListaProductos_SelectedIndexChanged);
            // 
            // lstColNombre
            // 
            this.lstColNombre.Text = "Nombre";
            this.lstColNombre.Width = 105;
            // 
            // lstColId
            // 
            this.lstColId.Text = "ID";
            this.lstColId.Width = 73;
            // 
            // lstColCosto
            // 
            this.lstColCosto.Text = "Costo";
            this.lstColCosto.Width = 85;
            // 
            // lstColPrecio
            // 
            this.lstColPrecio.Text = "Precio";
            this.lstColPrecio.Width = 76;
            // 
            // lstColPeso
            // 
            this.lstColPeso.Text = "Peso (g)";
            this.lstColPeso.Width = 74;
            // 
            // lstColFecha
            // 
            this.lstColFecha.Text = "Fecha de Compra";
            this.lstColFecha.Width = 147;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.label3);
            this.panel5.Location = new System.Drawing.Point(11, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(560, 44);
            this.panel5.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label3.Font = new System.Drawing.Font("Myanmar Text", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(205, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(179, 29);
            this.label3.TabIndex = 0;
            this.label3.Text = "Productos registrados";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // tabCalendario
            // 
            this.tabCalendario.Controls.Add(this.calendario);
            this.tabCalendario.Location = new System.Drawing.Point(4, 25);
            this.tabCalendario.Name = "tabCalendario";
            this.tabCalendario.Size = new System.Drawing.Size(896, 636);
            this.tabCalendario.TabIndex = 2;
            this.tabCalendario.Text = "Calendario";
            this.tabCalendario.ToolTipText = "Ver calendario";
            this.tabCalendario.UseVisualStyleBackColor = true;
            // 
            // calendario
            // 
            this.calendario.CalendarDimensions = new System.Drawing.Size(3, 4);
            this.calendario.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.calendario.FirstDayOfWeek = System.Windows.Forms.Day.Monday;
            this.calendario.Location = new System.Drawing.Point(40, 10);
            this.calendario.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.calendario.Name = "calendario";
            this.calendario.ShowWeekNumbers = true;
            this.calendario.TabIndex = 0;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(932, 692);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Microsoft YaHei", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximumSize = new System.Drawing.Size(952, 736);
            this.MinimumSize = new System.Drawing.Size(952, 736);
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Punto de venta - Principal";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Main_FormClosed);
            this.Load += new System.EventHandler(this.Main_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabVentas.ResumeLayout(false);
            this.splVentas.Panel1.ResumeLayout(false);
            this.splVentas.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splVentas)).EndInit();
            this.splVentas.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.tabProductos.ResumeLayout(false);
            this.splProductos.Panel1.ResumeLayout(false);
            this.splProductos.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splProductos)).EndInit();
            this.splProductos.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.tabCalendario.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabVentas;
        private System.Windows.Forms.TabPage tabProductos;
        private System.Windows.Forms.TabPage tabCalendario;
        private System.Windows.Forms.SplitContainer splVentas;
        private System.Windows.Forms.MonthCalendar calendario;
        private System.Windows.Forms.SplitContainer splProductos;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnCrearProd;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCostoProd;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtIdProd;
        private System.Windows.Forms.TextBox txtNomProd;
        private System.Windows.Forms.TextBox txtPeso;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ListView ListaProductos;
        private System.Windows.Forms.ColumnHeader lstColNombre;
        private System.Windows.Forms.ColumnHeader lstColId;
        private System.Windows.Forms.ColumnHeader lstColCosto;
        private System.Windows.Forms.ColumnHeader lstColPrecio;
        private System.Windows.Forms.ColumnHeader lstColPeso;
        private System.Windows.Forms.ColumnHeader lstColFecha;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtDesc;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtShowDesc;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}