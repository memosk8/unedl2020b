﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace PuntoDeVenta
{
    public partial class Main : Form
    {
        List<Producto> productos = new List<Producto>();

        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void splitContainer1_Panel1_Paint_1(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (MessageBox.Show("Confirmar salida", "Exit", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        //private async void WriteCharacters()
        //{
        //    using (StreamWriter writer = File.CreateText("Productos.txt"))
        //    {
        //        writer.Write("Nombre : " + prod.getNombre());
        //        await writer.WriteLineAsync("ID : " + prod.getId());
        //        await writer.WriteLineAsync("Peso (g) : " + prod.getPeso());
        //        await writer.WriteLineAsync("Costo : " + prod.getCosto());
        //        await writer.WriteLineAsync("Precio de venta : " + prod.getPrecio());
        //    }
        //}

        // crear producto 

        
        private void btnCrearProd_Click_1(object sender, EventArgs e)
        {
            Producto prod = new Producto();

            prod.setIdProducto(txtIdProd.Text);

            string path = @"Productos\" + prod.getId() + ".txt";
            if (!File.Exists(path))
            {
                prod.setCosto(double.Parse(txtCostoProd.Text));
                prod.setNombre(txtNomProd.Text);
                prod.setPeso(double.Parse(txtPeso.Text));
                prod.setPrecioVenta();
                prod.setDescripcion(txtDesc.Text);

                var row = new string[] {
                    prod.getNombre(),
                    prod.getId(),
                    prod.getCosto().ToString(),
                    prod.getPrecio().ToString(),
                    prod.getPeso().ToString(),
                    prod.getTimeStamp().ToString()
                };

                var lvi = new ListViewItem(row);
                lvi.Tag = prod;
                ListaProductos.Items.Add(lvi);
                txtShowDesc.Text = prod.getDescripcion();

                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine("Nombre : " + prod.getNombre());
                    sw.WriteLine("ID : " + prod.getId());
                    sw.WriteLine("Peso (g) : " + prod.getPeso());
                    sw.WriteLine("Costo : " + prod.getCosto());
                    sw.WriteLine("Precio de venta : " + prod.getPrecio());
                }

                MessageBox.Show("Producto registrado correctamente");
            }
            else
            {
                MessageBox.Show("El #ID ya existe en el registro,\n proporcione uno distinto");
            }
            //WriteCharacters();
            
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {

        }

        public void ListaProductos_SelectedIndexChanged(object sender, EventArgs e)
        {

            
        }

        private void txtShowDesc_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtNomProd_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
