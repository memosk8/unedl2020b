﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace TPV2doParcial
{
    public partial class Form1 : Form
    {
        private List<string> param = new List<string>();

        private static string path = @"segundoexamenparcial.txt";

        private readonly string[] parameters = {
                "LOCALIDADINVENTARIO", "install.option",
                "LOCALIDADBASE",       "install.port",
                "install.address",     "install.password", "install.redundancia"
        };

        public Form1()
        {
            InitializeComponent();
        }

        private void btnGetTextUrl_Click(object sender, EventArgs e)
        {
            try
            {
                StreamReader sr = new StreamReader(path);
                MessageBox.Show("File " + path + " opened succesfully !");
                //begin reading line per line
                string line = sr.ReadLine();
                int i = 0;
                while (line != null)
                {
                    if (line.Contains("="))
                    {
                        //will be written and added to cbxOptions only if line contains '='
                        Console.WriteLine(line);
                        param.Add(line);
                        listParam.Items.Add(param[i]);
                        i++;
                    }
                    line = sr.ReadLine();
                }
                sr.Close();
            }
            catch (IOException eo)
            {
                Console.WriteLine("The file could not be read:");
                MessageBox.Show("File " + path + " unable to open !");
                Console.WriteLine(eo.Message);
                Console.ReadKey();
            }
            btnReadFile.Enabled = false;
        }

        //private void writeTxt()
        //{
        //    Console.WriteLine("Escribiendo en archivo...");
        //    StreamWriter sw = new StreamWriter(path);
        //}

        //private void readTxt()
        //{
        //    Console.WriteLine("\nLeyendo archivo...\n");
        //    StreamReader sr = new StreamReader(path);

        //    // obtains all the text and displays into the txtbox
        //    txtShow.Text = sr.ReadToEnd();

        //    //begin reading line per line
        //    string line = sr.ReadLine();

        //    while (line != null)
        //    {
        //        Console.WriteLine(line);
        //        line = sr.ReadLine();
        //    }
        //    sr.Close();
        //}

        private void readSpecificTxt(string text)
        {

        }

        private void txtShow_TextChanged(object sender, EventArgs e)
        {

        }

        private void listParam_SelectedValueChanged(object sender, EventArgs e)
        {

        }

        private void showOptions(string opc)
        {
            if (listParam.SelectedItem.ToString().Contains(opc))
            {
                cbxOptions.Items.Clear();
                string[] split = listParam.SelectedItem.ToString().Split('=');
                lblSelected.Text = split[0];
                cbxOptions.Items.Add(split[1]);
            }
        }

        private void listParam_SelectedIndexChanged(object sender, EventArgs e)
        {

            showOptions(parameters[0]);

            if (listParam.SelectedItem.ToString().Contains(parameters[1]))
            {
                cbxOptions.Items.Clear();
                string[] split = listParam.SelectedItem.ToString().Split('=');
                lblSelected.Text = split[0];
                cbxOptions.Text = "FULL, instalación total (por defecto)";
                cbxOptions.Items.Add(" FULL, instalación total(por defecto)");
                cbxOptions.Items.Add(" PART, instalación parcial");
                cbxOptions.Items.Add(" MIN, instalacion mínima");
            }

            showOptions(parameters[2]);
            showOptions(parameters[3]);
            showOptions(parameters[4]);
            showOptions(parameters[5]);

            if (listParam.SelectedItem.ToString().Contains(parameters[6]))
            {
                cbxOptions.Items.Clear();
                string[] split = listParam.SelectedItem.ToString().Split('=');
                lblSelected.Text = split[0];
                cbxOptions.Text = " NORMAL, redundancia normal";
                cbxOptions.Items.Add(" NORMAL, redundancia normal");
                cbxOptions.Items.Add(" HIGH, alta redundancia");
                cbxOptions.Items.Add(" LOW, baja redundancia");
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
