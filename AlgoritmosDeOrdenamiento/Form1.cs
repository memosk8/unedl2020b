﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace AlgoritmosDeOrdenamiento
{
    public partial class window : Form
    {
        private List<int> Milista = new List<int>();
        private List<int> listaOrdenada;
        private readonly string[] algoritmos = { "Quick Sort", "Bubble Sort", "Merge Sort", "Heap sort" };
    
        public window()
        {
            InitializeComponent();
        }

        private List<int> QuickSort(List<int> lst)
        {
            if (lst.Count <= 1)
                return lst;
            int pivotIndex = lst.Count / 2;
            int pivot = lst[pivotIndex];
            List<int> left = new List<int>();
            List<int> right = new List<int>();

            for (int i = 0; i < lst.Count; i++)
            {
                if (i == pivotIndex) continue;

                if (lst[i] <= pivot)
                {
                    left.Add(lst[i]);
                }
                else
                {
                    right.Add(lst[i]);
                }
            }

            List<int> sorted = QuickSort(left);
            sorted.Add(pivot);
            sorted.AddRange(QuickSort(right));
            return sorted;
        }

        private List<int> BubbleSort(List<int> lst)
        {
            int temp;
            for (int j = 0; j <= lst.Count - 2; j++)
            {
                for (int i = 0; i <= lst.Count - 2; i++)
                {
                    if (lst[i] > lst[i + 1])
                    {
                        temp = lst[i + 1];
                        lst[i + 1] = lst[i];
                        lst[i] = temp;
                    }
                }
            }
            return lst;
        }

        private List<int> MergeSort(List<int> unsorted)
        {
            if (unsorted.Count <= 1)
                return unsorted;

            List<int> left = new List<int>();
            List<int> right = new List<int>();

            int middle = unsorted.Count / 2;
            for (int i = 0; i < middle; i++)  //Dividing the unsorted list
            {
                left.Add(unsorted[i]);
            }
            for (int i = middle; i < unsorted.Count; i++)
            {
                right.Add(unsorted[i]);
            }

            left = MergeSort(left);
            right = MergeSort(right);
            return Merge(left, right);
        }

        private List<int> Merge(List<int> left, List<int> right)
        {
            List<int> result = new List<int>();

            while (left.Count > 0 || right.Count > 0)
            {
                if (left.Count > 0 && right.Count > 0)
                {
                    if (left.First() <= right.First())  //Comparing First two elements to see which is smaller
                    {
                        result.Add(left.First());
                        left.Remove(left.First());      //Rest of the list minus the first element
                    }
                    else
                    {
                        result.Add(right.First());
                        right.Remove(right.First());
                    }
                }
                else if (left.Count > 0)
                {
                    result.Add(left.First());
                    left.Remove(left.First());
                }
                else if (right.Count > 0)
                {
                    result.Add(right.First());

                    right.Remove(right.First());
                }
            }
            return result;
        }

        //btn ordenar
        private void button2_Click(object sender, EventArgs e)
        {
            int i;
            if (Milista.Count >= 2)
            {
                btnLimpiar.Enabled = true;
            }
            if (cbxAlgo.SelectedItem == null)
            {
                MessageBox.Show("Selecciona un metodo de ordenamiento de la lista");
            }
            else if (cbxAlgo.SelectedItem.ToString() == algoritmos[0]) // "Quick Sort"
            {
                listBox1.Items.Clear();
                this.listaOrdenada = QuickSort(Milista);

                for (i = 0; i <= listaOrdenada.Count - 1; i++)
                {
                    listBox1.Items.Add(listaOrdenada.ElementAt(i));
                }
            }
            else if (cbxAlgo.SelectedItem.ToString() == algoritmos[1]) // "Bubble Sort"
            {
                listBox1.Items.Clear();
                this.listaOrdenada = BubbleSort(Milista);

                for (i = 0; i <= listaOrdenada.Count - 1; i++)
                {
                    listBox1.Items.Add(listaOrdenada.ElementAt(i));
                }
            }
            else if (cbxAlgo.SelectedItem.ToString() == algoritmos[2]) // "Merge Sort"
            {
                listBox1.Items.Clear();
                this.listaOrdenada = MergeSort(Milista);

                for (i = 0; i <= listaOrdenada.Count - 1; i++)
                {
                    listBox1.Items.Add(listaOrdenada.ElementAt(i));
                }
            }
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if(Milista.Count >= 2)
            {
                btnOrdenar.Enabled = true;
            }
            if (txtValor.TextLength == 0)
                MessageBox.Show("Ingresa un valor");
            else
            {
                try
                {
                    int n;
                    n = int.Parse(txtValor.Text);
                    Milista.Add(n);
                    txtValor.Clear();
                    txtValor.Focus();
                }
                catch (System.FormatException)
                {
                    txtValor.Clear();
                }
            }
        }

        private void window_Load(object sender, EventArgs e)
        {

        }

        private void txtNumero_TextChanged(object sender, EventArgs e)
        {

        }

        private void cbxAlgo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            txtValor.MaxLength = 6;
        }

        //btn limpiar List<int> y listbox
        private void button1_Click(object sender, EventArgs e)
        {
            if(listBox1.Items.Count > 0)
            {
                listBox1.Items.Clear();
                Milista.Clear();
                MessageBox.Show("Lista de valores reestablecida");
            }
        }
    }
}
