﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Veterinaria
{
    public class Ave : Mascota
    {

        private const string tipoAnimal = "Ave";

        public Ave() { }

        public Ave(string nom, string raza, float edad, Inform info) : base(nom, raza, edad, info)
        { }

        public string getTipo() => tipoAnimal;

        public override string ToString()
        {
            return "\n\tTipo de animal : " + getTipo() + base.ToString();
        }
    }
}
