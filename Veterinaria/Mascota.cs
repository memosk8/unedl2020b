﻿namespace Veterinaria
{
    public struct Inform
    {
        readonly string nombreDueño;
        readonly string numTel;
        readonly double cuota;

        public Inform(string n, string num, double c) {
            nombreDueño = n;
            numTel = num;
            cuota = c;
        }
        public string getNombreDueño() => nombreDueño;
        public string getNumTel() => numTel;
        public double getCuota() => cuota;
        public override string ToString()
        {
            return "\n\tNombre de Dueño : " + nombreDueño
                 + "\n\tNumero de telefono : " + numTel
                 + "\n\tCuota mensual : " + cuota;
        }

    }

    public class Mascota
    {
        private string nombre;
        private string raza;
        private float edad;
        public Inform inf;

        /* public struct Info {
             string nombreDueño;
             string numTel;
             double cuota;

             public Info(string n, string num, double c)
             {
                 nombreDueño = n;
                 numTel = num;
                 cuota = c;
             }           
             public void setNombre{ get; set; }
         } */  // unable to make struct work as attribute  :'(

        public Mascota(string nombre, string raza, float edad, Inform info)
        {
            this.nombre = nombre;
            this.raza = raza;
            this.edad = edad;
            this.inf = info;
        }

        public Mascota() { }

        public string Nombre { get; set; }

        public string Raza { get; set; }

        public float Edad { get; set; }

        public string getNombreDueño() => inf.getNombreDueño();

        public string getNumTel() => inf.getNumTel();

        public double getCuota() => inf.getCuota();

        public override string ToString()
        {
            return "\n\tNombre de mascota : " + this.nombre
                 + "\n\tRaza de animal : " + this.raza
                 + "\n\tEdad de la mascota : " + this.edad + " años"
                 + inf.ToString();
        }

    }
}
