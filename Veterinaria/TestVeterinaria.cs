﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Veterinaria
{
    public class TestVeterinaria : Collection<Mascota>
    {
        private static List<Mascota> Mascotas = new List<Mascota>();
        static void Main(string[] args)
        {
            int opc;
            do
            {
                opc = menu();
                switch (opc)
                {
                    case 1:
                        if (agregar())
                            Console.WriteLine("\n\n<Mascota agregada correctamente>");
                        else
                            Console.WriteLine("\nNo se pudo agregar la mascota\n");
                    break;

                    case 2:
                        buscar();
                    break;
                }
            } while (opc != 0);
        }

        private static void searchByName(string nom, int opc)
        {
            foreach (Mascota mascota in Mascotas)
            {
                if(opc == 1)
                {
                    if (mascota.Nombre == nom)
                    {
                        Console.WriteLine("- Datos de la mascota -");
                        Console.WriteLine(mascota);
                        Console.ReadKey();
                    }
                    else
                    {
                        Console.WriteLine("La mascota no existe en el registro");
                        Console.ReadKey();
                    }
                }
                if(opc == 2)
                {
                    if (mascota.inf.getNombreDueño() == nom)
                    {
                        Console.WriteLine("- Datos de la mascota -");
                        Console.WriteLine(mascota);
                        Console.ReadKey();
                    }
                    else
                    {
                        Console.WriteLine("La mascota no existe en el registro");
                        Console.ReadKey();
                    }
                }  
            }
        }

        private static void buscar()
        {
            char opc;
            do
            {
                Console.Clear();
                Console.WriteLine("\n\t--Buscar Mascotas--");
                Console.WriteLine("0. Regresar..");
                Console.WriteLine("1. Buscar por nombre de mascota");
                Console.WriteLine("2. Buscar por nombre de dueño");
                opc = char.Parse(Console.ReadLine());

                switch (opc)
                {
                    case '1':
                        Console.WriteLine("Ingresa el nombre de la mascota : ");
                        searchByName(Console.ReadLine(),1);
                    break;

                    case '2':
                        Console.WriteLine("Ingresa el nombre del dueño : ");
                        searchByName(Console.ReadLine(),2);
                    break;

                    default: break;
                }
            } while (opc != '0');
        }

        private static bool agregar()
        {
            bool done = false;
            char opc;
            string tipo="";

            do
            {
                Console.Clear();
                Console.WriteLine("Tipo de animal : ");
                Console.WriteLine("1. Perro\n2. Gato\n3. Ave\n");
                opc = char.Parse(Console.ReadLine());
                switch (opc)
                {
                    case '1': tipo = "perro"; break;
                    case '2': tipo = "gato"; break;
                    case '3': tipo = "ave"; break;
                    default:
                        Console.Clear();
                        Console.WriteLine("<-opcion invalida->");
                        break;
                }
            } while (opc < '0' || opc > '3');
                
            Console.WriteLine("Nombre de la mascota : ");
            string n = Console.ReadLine();
            Console.WriteLine("Raza : ");
            var raza = Console.ReadLine();
            Console.WriteLine("Edad : ");
            float edad = float.Parse(Console.ReadLine());
            Console.WriteLine("Nombre del dueño : ");
            string nombreD = Console.ReadLine();
            Console.WriteLine("Numero telefonico : ");
            string num = Console.ReadLine();
            Console.WriteLine("Cuota mensual : ");
            double cuota = double.Parse(Console.ReadLine());

            Inform inform = new Inform(nombreD, num, cuota);    // struct info

            switch (tipo)
            {
                case "perro":
                    Canino p = new Canino(n, raza, edad, inform);
                    Console.Clear();
                    Console.WriteLine("---Datos ingresados---");
                    Console.WriteLine(p);
                    Mascotas.Add(p);
                    Console.ReadKey();
                    done = true;
                break;

                case "gato":
                    Felino g = new Felino(n, raza, edad, inform);
                    Console.Clear();
                    Console.WriteLine("---Datos ingresados---");
                    Console.WriteLine(g);
                    Mascotas.Add(g);
                    Console.ReadKey();
                    done = true;
                break;

                case "ave":
                    Ave a = new Ave(n, raza, edad, inform);
                    Console.Clear();
                    Console.WriteLine("---Datos ingresados---");
                    Console.WriteLine(a);
                    Mascotas.Add(a);
                    Console.ReadKey();
                    done = true;
                break;

                default: break;
            }
            return done;
        }

        private static int menu()
        {
            int opc;
            while (true)
            {
                Console.Clear();
                Console.WriteLine("\n\tVeterinaria López\n");
                Console.WriteLine("0. Salir ");
                Console.WriteLine("1. Agregar nueva mascota ");
                Console.WriteLine("2. Buscar mascotas ");
                Console.WriteLine("3. Eliminar mascotas ");
                Console.Write("Selccion : ");
                if (int.TryParse(Console.ReadLine(), out opc))
                {
                    break;
                }
                Console.Clear();
                Console.WriteLine("Ingresa una opcion valida...");
                Console.ReadKey();
            }
            return opc;
        }
    }
}
